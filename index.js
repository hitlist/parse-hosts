'use strict'
/**
 * Helper to parse hosts strings for one or more services
 * @module allstar-auth/lib/parse-hosts
 * @since 0.1.0
 * @author Eric Satterwhite
 * @example parse('0.0.0.0:4929,nats://10.15.1.13:4222', 'nats://)
 * @example parse(['0.0.0.0:4929', 'nats://10.15.1.13:4222'], 'nats://)
 **/
const csv_exp = /\s?,\s?/g;

module.exports = ( hosts, protocol = 'nats://' ) => {
  return Array.isArray(hosts)
    ? hosts.map(parseItem(protocol))
    : parse(hosts, protocol)
}

function parse(str, protocol) {
  if (typeof str !== 'string') {
    throw new TypeError('nats hosts must be a string');
  }
  const items = str.split(csv_exp);
  return items.map(parseItem(protocol));
}

function parseItem(protocol) {
  return function(str) {
    return str.indexOf(protocol) === 0 ? str : `${protocol}${str}`;
  }
}
