# Parse Hosts

Simple parser for host strings

```javascript
const parse = require('@allstar/parse-hosts')
parse('0.0.0.0:4222,10.0.2.15:4931,nats:/192.141.0.1:5543', 'nats://')
[
  'nats://0.0.0.0:4222'
, 'nats://10.0.2.15:4931'
, 'nats://192.141:0.1:5543'
]
```
